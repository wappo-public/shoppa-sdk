# Shoppa SDK
Unofficial php libary for integration with Shoppa.

Developed by Wappo AB.

## Usage
This libary is used to upload products to shoppa mediablob. An already uploaded product can be modefied by reuploading it using the same ID.
```php
<?php
// ==== Load required files ===
require_once '/vendor/wappo/shoppa-sdk/load.php';

// Tap into the sdk namespace
use wappo\shoppa_sdk\Client as ShoppaClient;
use wappo\shoppa_sdk\Product as ShoppaProduct;


// === Create a client ===
$client = null;
try {
	$client = new ShoppaClient($username, $password, $customerId); // Credentials provided by shoppa 
} catch (SoapFault $ex) {
	// ...
}


// === Upload Image ===

// Get bytestream of image
$imagedata = file_get_contents("test.jpeg");

$imageResponse = null;
try {
	$imageResponse = $client->uploadImage($imagedata, "someimage");
	if ($response == null) { // Image data was faulty
		// ...
	}
} catch (SoapFault $ex) {
	// ...
}
// $imageResponse is now a hash used by shoppa mediablob to represent the image


// === Upload product ===
$product = new ShoppaProduct("1234sgdf_some_id", "Some Name");
$product->setText("ordinaryText", "lorem <h1> hello </h1> \n ipsum dolar sit amet....");
$product->setText("ordinaryText2", "ipsum lorem dolar sit amet....");
$product->addPicture($imageResponse);

$productResponse = null;
try {
	$productResponse = $client->uploadProduct($product);
} catch (SoapFault $ex) {
	...
}
// $productResponse is true on sucess otherwise holds an XML-error

```

## Development 
1. create `test/secrets.php` from `test/secrets.php.default`
2. run `docker-compose up`
3. visit `localhost:8080/test` for usefull debug infomation
4. develop on files in `src/*` and add more usefull print to `test/*`

### Todo
* Investigate what `$PRODUCT_ID_TYPE` in `product.php` should be
	* Is now set to `"Code1"` as in the productXML example
* Do more extensive testing of text uploading
	* Find out if they actualy get uploaded
	* Make sure it works with special characters and double escapes
	* Find out if empty response actually means success when uploadinga product in `client.php`

## Resources
* Shoppa SOAP API: http://wstest.mediablob.com/services/Products.asmx
* PHP SOAP Client: https://secure.php.net/manual/en/class.soapclient.php
* Example of PHP SOAP client usage: https://stackoverflow.com/questions/11593623/how-to-make-a-php-soap-call-using-the-soapclient-class
* Usefull UI for exploring SOAP API's: `SOAPUI`

###Example Product xml
```xml
<mediablob xmlns="http://shoppa.com/mediablobSchema" customerID="3745" createDate="2016-11-27T18:46:44">
	<products>
		<product id="HOX750F" idType="Code1">
			<texts>
				<text countryCode="se" languageCode="sv-SE" productName="Temporary name" brand="Temporary brand">
					<field name="DR-tidningstext" value="Från små till stora kokzoner. FlexiBridge-funktionen på induktionshällen kombinerar på ett smart sätt upp till fyra kokzoner för att skapa en extrastor kokzon som gör värmefördelningen helt enhetlig för alla former och storlekar på kastruller och pannor."/>
					<field name="Projekt" value="1703"/>
					<field name="salong" value="InfiFlex induktionshäll med 2 Infinite-zoner och en FlexiBridge-zon som anpassar sig efter kärlet och gör det möjligt att använda nästan vilka storlekar som helst. Med ProCook-funktionen kan du justera temperaturen bara genom att flytta kärlet! "/>
				</text>
			</texts>
			<pictures>
				<picture countryCode="se" languageCode="sv-SE" fieldName="productName" hashCode="a0fb5ec6-bf12-b76c-cd78-3bb3033b80c6"/>
			</pictures>
		</product>
	</products>
</mediablob>
```