<?php
namespace wappo\shoppa_sdk;
/**
 * Mediablob Soap client
 *
 * Example usage:
 *   1. Initiate client with auth details
 *   2. Upload images
 *   3. Create and upload a product
 */
class Client {
	private static $ENDPOINT_URL_WSDL_TEST = "http://wstest.mediablob.com/services/Products.asmx?WSDL";
	private static $ENDPOINT_URL_WSDL = "http://ws.mediablob.com/services/Products.asmx?WSDL";

	private $soapClient;
	private $customerID;

	/**
	 * Client initiation
	 *
	 * Params:
	 * 		Login credentials provided by Shoppa
	 * Throws:
	 * 		SoapFault:		Soap request error
	 */
	public function __construct($username, $password, $customerID, $testEnvironment = false) {
		// save customerID for xml generation
		$this->customerID = $customerID;

		// Create soap client
		if ($testEnvironment) {
			$this->soapClient = new \SoapClient(self::$ENDPOINT_URL_WSDL_TEST, array('trace' => 1));
		} else {
			$this->soapClient = new \SoapClient(self::$ENDPOINT_URL_WSDL);
		}

		// Create mediablob auth header
		$authHeader = new AuthHeader($username, $password, $customerID);

		// Set Soap Header
		$header = new \SoapHeader(
			"http://www.mediablob.com/", //namespace, defined by WSDL
			"AuthHeader", // header Name
			$authHeader, // Header
			true// MustUnderastand
		);
		$this->soapClient->__setSoapHeaders($header);
	}

	/**
	 * Uploads an image to mediablob
	 *
	 * Params:
	 *		$image: 		Image stream
	 *		$fileName: 		Image name
	 * Returns:
	 * 		On Succes: 		Image hash (string) to be used for product upload
	 *      On failure: 	Null
	 * Throws:
	 *		SoapFault: 		Soap request error
	 */
	public function uploadImage($image, $fileName) {
		// Build request body
		$params = new UploadImage($image, $fileName);

		// Send request
		$result = $this->soapClient->UploadImage($params);

		// Return result
		$guid = (!is_null($result) && isset($result->UploadImageResult)) ? $result->UploadImageResult : null;

		// 0-hash if something was wrong with image
		$guid = ($guid == "00000000-0000-0000-0000-000000000000") ? null : $guid;

		return $guid;
	}

	/**
	 * Uploads a product to mediablob
	 *
	 * Params:
	 *		$product: 		Mediaplob product object
	 * Returns:
	 * 		On Succes: 		true
	 *      On failure: 	XML error
	 * Throws:
	 *		SoapFault: 		Soap request error
	 */
	public function uploadProduct(Product $product) {
		// Build request body
		$params = new UploadProductXml($product->getXML($this->customerID));

		// Send request
		$result = $this->soapClient->UploadProductXml($params);

		// Extract result string
		$resultString = !(is_null($result) && isset($result->UploadProductXmlResult)) ? $result->UploadProductXmlResult : null;

		// Return result
		return ($resultString == '') ? true : $resultString;
	}

	/* Following functions are only for testing purposes */
	function __getLastRequest() {
		return $this->soapClient->__getLastRequest();
	}

	function __getSoapDataTypes() {
		return $this->soapClient->__getTypes();
	}

	function __getSoapFunctions() {
		return $this->soapClient->__getFunctions();
	}
}

/**
 * Help classes.
 *
 * They represent data types in the mediblob soap specification
 */
class AuthHeader {
	function __construct($username, $password, $customerId, $customerIdType = "ShoppaID") {
		$this->username = $username;
		$this->password = $password;
		$this->customerId = $customerId;
		$this->customerIdType = $customerIdType;
	}
}

class UploadImage {
	function __construct($blob, $fileName) {
		$this->blob = $blob;
		$this->fileName = $fileName;
	}
}

class UploadProductXml {
	function __construct($productXml) {
		$this->productXml = $productXml;
	}
}