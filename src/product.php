<?php
namespace wappo\shoppa_sdk;
/**
 *	Represents a mediablob product
 */
class Product {

	private $PRODUCT_ID_TYPE = "Code1";
	private $XMLNS = "http://shoppa.com/mediablobSchema";

	private $productID;
	private $name;
	private $brand;
	private $pictures;
	private $texts;

	function __construct(
		$productID,
		$name,
		$pictures = array(),
		$texts = array(),
		$brand = "Electrolux") {

		$this->productID = $productID;
		$this->name = $name;
		$this->pictures = $pictures;
		$this->texts = $texts;
		$this->brand = $brand;
	}

	/**
	 * Setters and getters for simple variables
	 */
	public function getProductID() {
		return $this->productID;
	}
	public function setProductID($productID) {
		$this->productID = $productID;
	}
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function geBrand() {
		return $this->brand;
	}
	public function setBrand($brand) {
		$this->brand = $brand;
	}

	/**
	 * Picture manupilation methods
	 *
	 * Notice: These methods only handle hash codes
	 * They are not capable of uploading images to the mediablob database
	 */
	public function addPicture($guid) {
		array_push($this->pictures, $guid);
	}
	public function getPictures() {
		return $this->pictures;
	}
	public function removePicture($guid) {
		foreach ($this->pictures as $key => $value) {
			if ($value == $guid) {
				unset($this->pictures[$key]);
				return true;
			}
		}
		return false;
	}
	public function clearPictures() {
		$this->pictures = array();
	}

	/**
	 * Text manipulation methods
	 *
	 * Used to set and modify various text properties of the product
	 **/
	public function setText($name, $value) {
		$this->texts[$name] = $value;
	}

	public function clearTexts() {
		$this->texts = array();
	}
	public function removeText($name) {
		unset($this->texts[$name]);
	}
	public function getText($name) {
		return $this->texts[$name];
	}
	public function getTexts() {
		return $this->texts;
	}

	/**
	 * This method returns a plain text XML representation of the product.
	 *
	 * The XML follows http://www3.shoppa.com/schemas/mediablob-v1.7.xsd and can be used for mediablob uploads
	 */
	public function getXML($customerID) {

		date_default_timezone_set(date_default_timezone_get());
		$date = date('Y-m-d\TH:i:s', time());

		$xw = new \XMLWriter();
		$xw->openMemory();
		$xw->startDocument("1.0", "utf-8");

		// Code blocks and indention hopefully makes the code a bit more readable
		$xw->startElement("mediablob");
		{
			$xw->startAttribute("customerID");
			{
				$xw->text($customerID);
			}
			$xw->endAttribute(); // customerID
			$xw->startAttribute("createDate");
			{
				$xw->text($date);
			}
			$xw->endAttribute(); // createDate
			$xw->startAttribute("xmlns");
			{
				$xw->text($this->XMLNS);
			}
			$xw->endAttribute(); // xmlns
			$xw->startElement("products");
			{
				$xw->startElement("product");
				{
					$xw->startAttribute("id");
					{
						$xw->text($this->productID);
					}
					$xw->endAttribute(); // id
					$xw->startAttribute("idType");
					{
						$xw->text($this->PRODUCT_ID_TYPE);
					}
					$xw->endAttribute(); // idType
					$xw->startElement("texts");
					{
						$xw->startElement("text"); // Only for sv-SE locale
						{
							$xw->startAttribute("countryCode");
							{
								$xw->text("se");
							}
							$xw->endAttribute(); //countryCode
							$xw->startAttribute("languageCode");
							{
								$xw->text("sv-SE");
							}
							$xw->endAttribute(); //languageCode
							$xw->startAttribute("productName");
							{
								$xw->text($this->name);
							}
							$xw->endAttribute(); //productName
							$xw->startAttribute("brand");
							{
								$xw->text($this->brand);
							}
							$xw->endAttribute(); //brand
							foreach ($this->texts as $name => $value) {
								$xw->startElement("field");
								{
									$xw->startAttribute("name");
									{
										$xw->text($name);
									}
									$xw->endAttribute(); // name
									$xw->startAttribute("value");
									{
										$xw->text($value);
									}
									$xw->endAttribute(); // value
								}
								$xw->endElement(); // field
							}
						}
						$xw->endElement(); // text
					}
					$xw->endElement(); //texts
					if (count($this->pictures) > 0) {
						$xw->startElement("pictures");
						{
							foreach ($this->pictures as $key => $guid) {
								$xw->startElement("picture");
								{
									$xw->startAttribute("countryCode");
									{
										$xw->text("se");
									}
									$xw->endAttribute(); // countryCode
									$xw->startAttribute("languageCode");
									{
										$xw->text("sv-SE");
									}
									$xw->endAttribute(); //languageCode
									$xw->startAttribute("fieldName");
									{
										$xw->text($this->name);
									}
									$xw->endAttribute(); // fieldName
									$xw->startAttribute("hashCode");
									{
										$xw->text($guid);
									}
									$xw->endAttribute(); // hashCode
								}
								$xw->endElement(); // picture
							}
						}
						$xw->endElement(); // pictures
					}
				}
				$xw->endElement(); //product
			}
			$xw->endElement(); // products
		}
		$xw->endElement(); // mediablob
		$xw->endDocument();
		return $xw->outputMemory();
	}
}