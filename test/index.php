<?php
require_once '../src/load.php';
require_once 'secrets.php';

// Tap into the sdk namespace
use wappo\shoppa_sdk\Client as ShoppaClient;
use wappo\shoppa_sdk\Product as ShoppaProduct;

// Helper function for pretty print
function prettyprint($var, $name = "someVar") {
	highlight_string("<?php\n\$" . $name . " =\n" . var_export($var, true) . ";\n?>\n");
}

$client = null;
try {
	$client = new ShoppaClient(
		$username,
		$password,
		$customerId,
		true// Test Environment
	);
} catch (SoapFault $ex) {
	print("Exception while sending image request: <br>");
	prettyprint($ex, "exception");
	exit("Exiting...");
}

$soapDataTypes = $client->__getSoapDataTypes();
prettyprint($soapDataTypes, "client->__getSoapDataTypes()");

$soapFunctions = $client->__getSoapFunctions();
prettyprint($soapFunctions, "client->__getSoapFunctions()");

$imagedata = file_get_contents("test.jpeg");

$response = null;
try {
	$response = $client->uploadImage($imagedata, "someimage");
	if ($response == null) {
		print("Image data was faulty<br>");
		exit("Exiting...");
	}
} catch (SoapFault $ex) {
	print("Exception while sending image request: <br>");
	prettyprint($ex, "exception");
	exit("Exiting...");
}

prettyprint($client->__getLastRequest(), "lastRequest");

prettyprint($response, "response");

$product = new ShoppaProduct("1234sgdf", "Some Name");
$product->setText("ordinaryText", "lorem <h1> hello </h1> \n ipsum dolar sit amet....");
$product->setText("ordinaryText2", "ipsum lorem dolar sit amet....");
$product->addPicture($response);

$productXML = $product->getXML($customerId);

prettyprint($productXML, "productXML");

$productResponse = null;
try {
	$productResponse = $client->uploadProduct($product);
} catch (SoapFault $ex) {
	print("Exception while sending product request: <br>");
	prettyprint($ex, "exception");
	exit("Exiting...");
}

prettyprint($client->__getLastRequest(), "lastRequest");

prettyprint($productResponse, "productResponse");
?>